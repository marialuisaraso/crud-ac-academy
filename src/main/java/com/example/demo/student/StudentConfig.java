package com.example.demo.student;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.util.List;


import static java.util.Calendar.MARCH;

@Configuration
public class StudentConfig {

    @Bean
    CommandLineRunner commandLineRunner (StudentRepository repository){
        return args -> {
            Student malu = new Student(

                    "malu",
                    "maria.raso@avenuecode.com",
                    LocalDate.of(2002, MARCH,26));

            Student ana = new Student(

                    "ana",
                    "ana@avenuecode.com",
                    LocalDate.of(2001, MARCH,16));

            repository.saveAll(List.of(malu,ana));
        };
    }
}

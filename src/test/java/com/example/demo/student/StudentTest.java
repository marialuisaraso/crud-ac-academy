package com.example.demo.student;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.Period;

import static java.time.Month.FEBRUARY;

@ExtendWith(MockitoExtension.class)
public class StudentTest {

    @Mock
    private StudentRepository studentRepository;

    @Test
    void getAge() {
        Student student = new Student(
                "malu",
                "maria.raso@avenuecode.com",
                LocalDate.of(2002, FEBRUARY, 26)
        );

        Assertions.assertEquals(Period.between(student.getDob(), LocalDate.now()).getYears(), student.getAge());
    }
}

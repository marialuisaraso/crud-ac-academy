package IntegrationTests;

import com.example.demo.student.Student;
import com.example.demo.student.StudentRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDate;
import java.util.Optional;

import static java.time.Month.FEBRUARY;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@DataJpaTest
public class StudentRepositoryIntegrationTests {

    @Autowired
    private StudentRepository underTest;

    @AfterEach
    void tearDown() {
        underTest.deleteAll();
    }

    @Test
    void checkIfStudentEmailExists() {
        // given
        Student student = new Student(
                "malu",
                "maria.raso@avenuecode.com",
                LocalDate.of(2002, FEBRUARY, 26)
        );

        underTest.save(student);

        // when
        Optional<Student> expected = underTest.findStudentByEmail(student.getEmail());

        // then
        assertThat(expected.isPresent()).isTrue();
    }

    @Test
    void checkIfStudentEmailDoesNotExists() {
        // given
        Student student = new Student(
                "malu",
                "maria.raso@avenuecode.com",
                LocalDate.of(2002, FEBRUARY, 26)
        );

        underTest.save(student);

        String email = "maria.raso@avenuecode.com";

        // when
        Optional<Student> expected = underTest.findStudentByEmail(email);

        // then
        assertThat(expected.isPresent()).isFalse();
    }
}